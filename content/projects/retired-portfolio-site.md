+++
demo_url = "https://dyllandry.github.io/final-portfolio-site"
description = "A retired portfolio site of mine built on a custom Webpack static site builder. A fantastic learning experience."
order = 3
repository_url = "https://github.com/dyllandry/final-portfolio-site"
thumbnail = "/uploads/screen-shot-1.png"
thumbnail_alt = "A screen shot of the retired portfolio site home page."
title = "Retired Portfolio Site"

+++
