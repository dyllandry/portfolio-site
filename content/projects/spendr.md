+++
demo_url = "https://youthful-lamport-5bf629.netlify.com/"
description = "🏃‍♂️High velocity online banking frontend prototype with React and Redux. Looks a bit like your email. To discourage over spending, all transactions must first be approved by you prior to processing."
order = 1
repository_url = "https://github.com/dyllandry/spendr"
thumbnail = "/uploads/7-12-19_screen-shot.png"
thumbnail_alt = "A screen shot of spendr."
title = "Spendr"

+++
